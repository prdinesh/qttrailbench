#include "dialog.h"
#include "ui_dialog.h"
#include <QCompleter>
#include <stdio.h>
#include <QTimer>

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
    ui->lineEdit_1->setPlaceholderText("One");
    ui->lineEdit_2->setPlaceholderText("Two");
    ui->lineEdit_3->setPlaceholderText("Three");
    ui->lineEdit_4->setPlaceholderText("Four");

    //Autocomplete
    QStringList mediList;
    mediList << "Afghanistan" << "Albania" << "Algeria" << "Andorra" << "Angola" << "Antigua and Barbuda" << "Argentina" << "Armenia" << "Aruba" << "Australia" << "Austria" << "Azerbaijan" << "Bahamas, The" << "Bahrain" << "Bangladesh" << "Barbados" << "Belarus" << "Belgium" << "Belize" << "Benin" << "Bhutan" << "Bolivia" << "Bosnia and Herzegovina" << "Botswana" << "Brazil" << "Brunei " << "Bulgaria" << "Burkina Faso" << "Burma" << "Burundi" << "Cambodia" << "Cameroon" << "Canada" << "Cape Verde" << "CentralAfrican Republic" << "Chad" << "Chile" << "China" << "Colombia" << "Comoros" << "Congo, Democratic Republic of the" << "Congo, Republic of the" << "Costa Rica" << "Cote d'Ivoire" << "Croatia" << "Cuba" << "Curacao" << "Cyprus" << "Czech Republic" << "Denmark" << "Djibouti" << "Dominica" << "Dominican Republic" << "Ecuador" << "Egypt" << "El Salvador" << "Equatorial Guinea" << "Eritrea" << "Estonia" << "Ethiopia" << "Fiji" << "Finland" << "France" << "Gabon" << "Gambia, The" << "Georgia" << "Germany" << "Ghana" << "Greece" << "Grenada" << "Guatemala" << "Guinea" << "Guinea-Bissau" << "Guyana" << "Haiti" << "Holy See" << "Honduras" << "Hong Kong" << "Hungary" << "Iceland" << "India" << "Indonesia" << "Iran" << "Iraq" << "Ireland" << "Israel" << "Italy" << "Jamaica" << "Japan" << "Jordan" << "Kazakhstan" << "Kenya" << "Kiribati" << "Kosovo" << "Kuwait" << "Kyrgyzstan" << "Laos" << "Latvia" << "Lebanon" << "Lesotho" << "Liberia" << "Libya" << "Liechtenstein" << "Lithuania" << "Luxembourg" << "Macau" << "Maedonia" << "Madagascar" << "Malawi" << "Malaysia" << "Maldives" << "Mali" << "Malta" << "Marshall Islands" << "Mauritania" << "Mauritius" << "Mexico" << "Micronesia" << "Moldova" << "Monaco" << "Mongolia" << "Montenegro" << "Morocco" << "Mozambique" << "Namibia" << "Nauru" << "Nepl" << "Netherlands" << "Netherlands Antilles" << "New Zealand" << "Nicaragua" << "Niger" << "Nigeria" << "North Korea" << "Norway" << "Oman" << "Pakistan" << "Palau" << "Palestinian Territories" << "Panama" << "Papua New Guinea" << "Paraguay" << "Peru" << "Philippines" << "Poland" << "Portugal" << "Qatar" << "Romania" << "Russia" << "Rwanda" << "Saint Kitts and Nevis" << "Saint Lucia" << "SaintVincent and the Grenadines" << "Samoa " << "San Marino" << "Sao Tome and Principe" << "Saudi Arabia" << "Senegal" << "Serbia" << "Seychelles" << "SierraLeone" << "Singapore" << "Sint Maarten" << "Slovakia" << "Slovenia" << "Solomon Islands" << "Somalia" << "SouthAfrica" << "South Korea" << "South Sudan" << "Spain " << "Sri Lanka" << "Sudan" << "Suriname" << "Swaziland " << "Sweden" << "Switzerland" << "Syria" << "Taiwan" << "Tajikistan" << "Tanzania" << "Thailand " << "Timor-Leste" << "Togo" << "Tonga" << "Trinidad and Tobago" << "Tunisia" << "Turkey" << "Turkmenistan" << "Tuvalu" << "Uganda" << "Ukraine" << "United Arab Emirates" << "United Kingdom" << "United States of America" << "Uruguay" << "Uzbekistan" << "Vanuatu" << "Venezuela" << "Vietnam" << "Yemen" << "Zambia" << "Zimbabwe";
    mediList.sort(Qt::CaseInsensitive);
    QCompleter *completer = new QCompleter(mediList, this);

    completer->setCaseSensitivity(Qt::CaseInsensitive);
    completer->setCompletionMode(QCompleter::UnfilteredPopupCompletion);
    completer->setModelSorting(QCompleter::CaseInsensitivelySortedModel);

    // Enable below to match the contains anywhere
    //completer->setFilterMode(Qt::MatchContains);

    ui->lineEdit_1->setCompleter(completer);
    ui->lineEdit_2->setCompleter(completer);
    ui->lineEdit_3->setCompleter(completer);
    ui->lineEdit_4->setCompleter(completer);

    //Enter Key Navigation
    QObject::connect(completer,SIGNAL(activated(const QString&)),this,SLOT(onSelected(const QString&)));
}

void Dialog::focusNext(void){
    this->focusNextChild();
}

void Dialog::onSelected(const QString& text){
    QTimer::singleShot(0, this, SLOT(focusNext()));
}

Dialog::~Dialog()
{
    delete ui;
}
